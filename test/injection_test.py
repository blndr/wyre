from unittest.mock import Mock

from test.fixtures import A, B, C, D, A2


class InjectionTest:
    def test_injection_of_two_dependencies(self):
        # when
        a = A()

        # then
        assert isinstance(a.b, B)
        assert isinstance(a.c, C)

    def test_injection_uniqueness(self):
        # given
        a1 = A()

        # when
        a2 = A()

        # then
        assert a1.b is not a2.b

    def test_injection_of_a_mocked_dependency(self):
        # given
        mocked_c = Mock(spec=C)

        # when
        a = A(other_dependency=mocked_c)

        # then
        assert isinstance(a.b, B)
        assert isinstance(a.c, Mock)

    def test_injection_of_mixed_args_and_kwargs_preserve_non_instantiable_attributes(self):
        # when
        a = A2('yo', number=3)

        # then
        assert isinstance(a.b, B)
        assert a.foo == 'bar'
        assert a.number == 3
        assert a.prefix == 'yo'

    def test_injection_of_diamond_like_dependencies_with_transitivity(self):
        # when
        a = A()

        # then
        assert isinstance(a.c.d, D)
        assert isinstance(a.b.d, D)
        assert a.b.d is not a.c.d
        assert a.b.d.e is not a.c.d.e
