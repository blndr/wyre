from pytest import raises

from wyre import inject, InjectionError


class InjectionErrorTest:
    def test_invalid_injection_of_a_single_dependency_outside_of_init(self):
        # given
        class B: pass

        # when
        with raises(InjectionError) as e:
            class A:
                @inject
                def change_dependency(self, dependency=B):
                    self.b = dependency

        # then
        assert str(e.value) == 'You can only inject dependencies by decorating __init__().\n' \
                               'You tried to inject on change_dependency'

    def test_invalid_injection_when_no_dependency_declared(self):
        # when
        with raises(InjectionError) as e:
            class A:
                @inject
                def __init__(self):
                    pass

        # then
        assert str(e.value) == 'Nothing to inject. Add dependencies or remove @inject decorator.'

    def test_invalid_injection_because_of_circular_dependencies(self):
        # given
        class A: pass

        class B: pass

        class C: pass

        _create_dependency(A, B)
        _create_dependency(B, C)
        _create_dependency(C, A)

        # when
        with raises(InjectionError) as e:
            A()

        # then
        assert str(e.value) == 'Circular dependency detected : A -> B -> C -> A'


def _create_dependency(from_class, to_class):
    @inject
    def __init__(self, dependency=to_class):
        self.dependency = dependency

    from_class.__init__ = __init__
