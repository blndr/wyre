from wyre import inject

"""
      A
    /   \
   B     C
    \   /
      D
      |
      E
"""


class E: pass


class D:
    @inject
    def __init__(self, dependency=E):
        self.e = dependency


class B:
    @inject
    def __init__(self, dependency=D):
        self.d = dependency


class C:
    @inject
    def __init__(self, dependency=D):
        self.d = dependency


class A:
    @inject
    def __init__(self, dependency=B, other_dependency=C):
        self.b = dependency
        self.c = other_dependency


class A2:
    @inject
    def __init__(self, prefix, foo='bar', number=2, dependency=B):
        self.b = dependency
        self.foo = foo
        self.number = number
        self.prefix = prefix
