# Download and tweak

You may git clone this project and make it better.
Any submitted merge request will be greatly appreciated :-)

### Install requirements
```
. venv/bin/activate
pip install -r requirements.txt
```

### Unit test execution
```
. venv/bin/activate
pytest
```

### Test coverage
```
. venv/bin/activate
pytest test --cov=app -v
```